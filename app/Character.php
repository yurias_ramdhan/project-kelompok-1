<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    protected $table = 'character';

    protected $fillable = ['nama', 'umur', 'bio'];

    public function peran()
    {
        return $this->hasMany('App\Peran');
    }
}
