<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = 'berita';

    protected $fillable = ['tulisan', 'gambar', 'rencana', ];

    public function peran()
    {
        return $this->hasMany('App\Peran');
    }
}
