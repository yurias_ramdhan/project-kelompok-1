<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Peran extends Model
{
    protected $table = 'peran';

    protected $fillable = ['film_id', 'character_id', 'nama'];

    public function film()
    {
        return $this->belongsTo('App\Film');
    }

    public function character()
    {
        return $this->belongsTo('App\Character');
    }
    

}
