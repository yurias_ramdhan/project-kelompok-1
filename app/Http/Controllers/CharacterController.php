<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Character;
use RealRashid\SweetAlert\Facades\Alert;

class CharacterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function create()
    {
        return view('character.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $character = new Character;

        $character->nama = $request->nama;
        $character->umur = $request->umur;
        $character->bio = strip_tags($request->bio);
        
        $character->save();
        Alert::success('Berhasil', 'Data Character Berhasil Ditambahkan');
        return redirect('/character');
    }

    public function index()
    {
        $character = Character::all();
        return view('character.index', compact('character'));
    }

    public function show($character_id)
    {
        $character = Character::where('id', $character_id)->first();
        return view('character.show', compact('character'));
    }

    public function edit($character_id)
    {
        $character = Character::where('id', $character_id)->first();
        return view('character.edit', compact('character'));
    }

    public function update(Request $request, $character_id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $character = Character::find($character_id);
        
        $character->nama = $request['nama'];
        $character->umur = $request['umur'];
        $character->bio = strip_tags($request['bio']);

        $character->save();

        Alert::success('Update', 'Data Character Berhasil Diupdate');
        return redirect('/character');
    }

    public function destroy($character_id)
    {
        $character = Character::find($character_id);

        $character->delete();

        Alert::success('Delete', 'Data Character Berhasil Dihapus');
        return redirect('/character');
    }
}
