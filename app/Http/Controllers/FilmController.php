<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Film;
use File;
use RealRashid\SweetAlert\Facades\Alert;


class FilmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::all();
        return view('film.index', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = DB::table('genre')->get();
        return view('film.create',compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'genre_id' => 'required',
        ]);

        $PosterName = time().'.'.$request->poster->extension();

        $request->poster->move(public_path('gambar'), $PosterName);


        $film = new Film;

        $film->judul = $request->judul;
        $film->ringkasan = strip_tags($request->ringkasan);
        $film->tahun = $request->tahun;
        $film->poster = $PosterName;
        $film->genre_id = $request->genre_id;

        $film->save();

        Alert::success('Berhasil', 'Data Film Berhasil Ditambahkan');
        return redirect('/film');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::findOrFail($id);

        return view('film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = DB::table('genre')->get();
        $film = Film::findOrFail($id);

        return view('film.edit', compact('film','genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'image|mimes:jpeg,png,jpg|max:2048',
            'genre_id' => 'required',
        ]);

        $film = Film::find($id);

        if($request->has('poster') ) {
            $PosterName = time().'.'.$request->poster->extension();

            $request->poster->move(public_path('gambar'), $PosterName);

            $film->judul = $request->judul;
            $film->ringkasan = strip_tags($request->ringkasan);
            $film->tahun = $request->tahun;
            $film->poster = $PosterName;
            $film->genre_id = $request->genre_id;

        }else {

            $film->judul = $request->judul;
            $film->ringkasan = strip_tags($request->ringkasan);
            $film->tahun = $request->tahun;
            $film->genre_id = $request->genre_id;
        }

        $film->update();
        Alert::success('Update', 'Data Film Berhasil Diupdate');
        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);

        $path = "gambar/";
        File::delete($path . $film->poster);

        $film->delete();

        Alert::success('Delete', 'Data Film Berhasil Dihapus');
        return redirect('/film');
    }
}
