<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;
use RealRashid\SweetAlert\Facades\Alert;

class GenreController extends Controller
{
    public function create()
    {
        return view('genre.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $genre = new Genre;

        $genre->nama = $request->nama;
        
        $genre->save();

        Alert::success('Berhasil', 'Data Genre Berhasil Ditambahkan');
        return redirect('/genre');
    }

    public function index()
    {
        $genre = Genre::all();
        return view('genre.index', compact('genre'));
    }

    public function show($genre_id)
    {
        $genre = Genre::where('id', $genre_id)->first();
        return view('genre.show', compact('genre'));
    }

    public function edit($genre_id)
    {
        $genre = Genre::where('id', $genre_id)->first();
        return view('genre.edit', compact('genre'));
    }

    public function update(Request $request, $genre_id)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $genre = Genre::find($genre_id);
        
        $genre->nama = $request['nama'];

        $genre->save();

        Alert::success('Update', 'Data Genre Berhasil Diupdate');
        return redirect('/genre');
    }

    public function destroy($genre_id)
    {
        $genre = Genre::find($genre_id);

        $genre->delete();

        Alert::success('Delete', 'Data Genre Berhasil Dihapus');
        return redirect('/genre');
    }
}
