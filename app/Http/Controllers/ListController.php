<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Link;
use File;
use RealRashid\SweetAlert\Facades\Alert;


class ListController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $list = List::all();
        // return view('list.index', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $genre = DB::table('genre')->get();
        // return view('list.create',compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'judul' => 'required',
        //     'ringkasan' => 'required',
        //     'tahun' => 'required',
        //     'poster' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        //     'genre_id' => 'required',
        // ]);

        // $PosterName = time().'.'.$request->poster->extension();

        // $request->poster->move(public_path('gambar'), $PosterName);


        // $list = new List;

        // $list->judul = $request->judul;
        // $list->ringkasan = $request->ringkasan;
        // $list->tahun = $request->tahun;
        // $list->poster = $PosterName;
        // $list->genre_id = $request->genre_id;

        // $list->save();

        // return redirect('/list/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $list = List::findOrFail($id);

        // return view('list.show', compact('list'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // $genre = DB::table('genre')->get();
        // $list = List::findOrFail($id);

        // return view('list.edit', compact('list','genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $request->validate([
        //     'judul' => 'required',
        //     'ringkasan' => 'required',
        //     'tahun' => 'required',
        //     'poster' => 'image|mimes:jpeg,png,jpg|max:2048',
        //     'genre_id' => 'required',
        // ]);

        // $list = List::find($id);

        // if($request->has('poster') ) {
        //     $PosterName = time().'.'.$request->poster->extension();

        //     $request->poster->move(public_path('gambar'), $PosterName);

        //     $list->judul = $request->judul;
        //     $list->ringkasan = $request->ringkasan;
        //     $list->tahun = $request->tahun;
        //     $list->poster = $PosterName;
        //     $list->genre_id = $request->genre_id;

        // }else {

        //     $list->judul = $request->judul;
        //     $list->ringkasan = $request->ringkasan;
        //     $list->tahun = $request->tahun;
        //     $list->genre_id = $request->genre_id;
        // }

        // $list->update();

        // return redirect('/list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $list = List::find($id);

        // $path = "gambar/";
        // File::delete($path . $list->poster);

        // $list->delete();

        // return redirect('/list');
    }
}
