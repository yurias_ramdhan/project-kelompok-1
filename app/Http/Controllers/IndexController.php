<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function home(){
        return view('home1');
    }

    public function data_tables(){
        return view('data-tables');
    }
}
