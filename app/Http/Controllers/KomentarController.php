<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Komentar;

use Illuminate\Http\Request;

class KomentarController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'content' => 'required',
            'point' => 'required',
        ]);

        $komentar = new Komentar;

        $komentar->user_id = Auth::id();
        $komentar->film_id = $request->film_id;
        $komentar->content = $request->content;
        $komentar->point = $request->point;

        $komentar->save();

        return redirect()->back();
    }
}
