<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Peran;

use Illuminate\Http\Request;

class PeranController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
        ]);

        $peran = new Peran;

        $peran->list_id = $request->list_id;
        $peran->character_id = $request->character_id;
        $peran->nama = $request->nama;

        $peran->save();

        return redirect()->back();
    }
}
