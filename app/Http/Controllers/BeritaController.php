<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;

class BeritaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function create()
    {
        return view('berita.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'tulisan' => 'required',
            'rencana' => 'required',
        ]);

        $berita = new Berita;

        $berita->tulisan = $request->tulisan;
        $berita->rencana = $request->rencana;
        
        $berita->save();
        return redirect('/berita');
    }

    public function index()
    {
        $berita = Berita::all();
        return view('berita.index', compact('berita'));
    }

    public function show($berita_id)
    {
        $berita = Berita::where('id', $berita_id)->first();
        return view('berita.show', compact('berita'));
    }

    public function edit($berita_id)
    {
        $berita = Berita::where('id', $berita_id)->first();
        return view('berita.edit', compact('berita'));
    }

    public function update(Request $request, $berita_id)
    {
        $request->validate([
            'tulisan' => 'required',
            'rencana' => 'required',
        ]);

        $berita = Berita::find($id);

            $berita->tulisan = $request->tulisan;
            $berita->rencana = $request->rencana;

        $berita->update();

        return redirect('/berita');
    }

    public function destroy($berita_id)
    {
        $berita = Berita::find($berita_id);

        $berita->delete();

        return redirect('/berita');
    }
}
