<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function welcome(Request $request){
        $namaDepan = $request['first'];
        $namaBelakang = $request['last'];
        return view('film', compact('namaDepan','namaBelakang'));
    }
}
