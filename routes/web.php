<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');


// CRUD Character
// Create
Route::get('/character/create', 'CharacterController@create'); // route menuju form create
Route::post('/character', 'CharacterController@store'); // route untuk menyimpan data ke database

// Read
Route::get('/character', 'CharacterController@index'); // route list character
Route::get('/character/{character_id}', 'CharacterController@show'); // route detail character

// Update
Route::get('/character/{character_id}/edit', 'CharacterController@edit'); // route menuju ke form edit
Route::put('/character/{character_id}', 'CharacterController@update'); // route untuk update data berdasarkan di database

// Delete
Route::delete('/character/{character_id}', 'CharacterController@destroy'); // route untuk hapus data di database


Route::group(['middleware' => ['auth']], function() {
    // CRUD Genre
    // Create
    Route::get('/genre/create', 'GenreController@create'); // route menuju form create
    Route::post('/genre', 'GenreController@store'); // route untuk menyimpan data ke database
    
    // Read
    Route::get('/genre', 'GenreController@index'); // route list cast
    Route::get('/genre/{genre_id}', 'GenreController@show'); // route detail genre
    
    // Update
    Route::get('/genre/{genre_id}/edit', 'GenreController@edit'); // route menuju ke form edit
    Route::put('/genre/{genre_id}', 'GenreController@update'); // route untuk update data berdasarkan di database
    
    // Delete
    Route::delete('/genre/{genre_id}', 'GenreController@destroy'); // route untuk hapus data di database

    // Update Profile
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);

    Route::resource('komentar', 'KomentarController')->only([
        'index', 'store'
    ]);

    Route::resource('peran', 'PeranController')->only([
        'index', 'store'
    ]);
});


    // Update Profile
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);

    Route::resource('komentar', 'KomentarController')->only([
        'index', 'store'
    ]);

    Route::resource('peran', 'PeranController')->only([
        'index', 'store'
    ]);




// CRUD Film
Route::resource('film', 'FilmController');
Auth::routes();

