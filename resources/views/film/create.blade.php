@extends('layout.master')
@section('judul')
Halaman Tambah film 
@endsection    
@section('content')


@push('style')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
  <style>

  </style>  
@endpush


@push('script')
<script src="https://cdn.tiny.cloud/1/yk0imf3zr5gkoy1klg43thkt9ooh0kfsw1w8nkvog26llb1z/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
  });
</script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});

</script>
@endpush


<div class="col-lg">
  <div class="login__form">

    <form action="/film" method="POST" enctype="multipart/form-data" >
      @csrf
      <div class="form-group">
        <label class="text-white">Judul film</label>
        <input type="text" name="judul" class="form-control" style="width:850px;">
      </div>
      @error('judul')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label class="text-white">Ringkasan film</label>
        <textarea name="ringkasan" cols="30" rows="10" class="form-control" style="width:850px;"></textarea>
      </div>
      @error('ringkasan')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label class="text-white">Tahun film</label>
        <input type="number" name="tahun" class="form-control" style="width:850px;">
      </div>
      @error('tahun')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label class="text-white">Poster film</label>
        <input type="file" name="poster" class="form-control" style="width:850px;">
      </div>
      @error('poster')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <label class="text-white">Genre film</label> 
      <div class="">
        <select name="genre_id" class="js-example-basic-single">
          <option value="" width='140px'>---Pilih Genre---</option>
          @foreach ($genre as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>
          @endforeach
  
          @error('genre_id')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          
        </select>
      </div>

      <div>
        <h1 class="text-danger" style='font-size:15px;'>*jika belum ada genre, silakan kunjungi halaman <a href='/genre' style="color:white; text-decoration: underline">genre</a></h1>
      </div>
      
      <div class="login__register">
        <button type="submit" class="primary-btn" style="margin-left: 317px; margin-top:50px;">Submit</button>
      </div>
    </form>
  </div>
</div>
@endsection



