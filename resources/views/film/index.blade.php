@extends('layout.master')
@section('judul')
Halaman film
@endsection    
@section('content')

@auth
 <div class="col-12" style="">
   <a href="/film/create" class="btn btn-primary my-2" style="display: block; width:80px;">Tambah</a>
  </div>     
@endauth

@forelse ($film as $item)
<div class="col-3">
  <div class="product__item" >
      <div class="product__item__pic" >
        <img src="{{asset('gambar/'. $item->poster)}}"  height='100%' style="border: 1px solid gray;">
      </div>
      <div class="product__item__text">
          <ul>
              <li >{{$item->genre->nama}}</li>
          </ul>
          <h5 class="mb-2 text-white">{{$item->judul}}</h5>
          <p class="card-text text-gray"> {{Str::limit($item->ringkasan, 30)}} </p>
          @auth
            <form action="/film/{{$item->id}}" method="POST">
              @csrf
              @method('DELETE')

                <a href="/film/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
          @endauth
        </div>
      </div>
      
    </div>
    
    @empty
      <h4 style="color:white;">Data film Belum Ada</h4>
      @endforelse  


@endsection

