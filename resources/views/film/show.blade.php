@extends('layout.master')
@section('judul')
Halaman Detail film {{$film->judul}}
@endsection    
@section('content')


<div style="display: block; text-align:center; margin-left:480px; margin-bottom:25px;">
  <img src="{{asset('gambar/'.$film->poster)}}" alt="anime" width="200px">
  <h1 class="text-white">{{$film->judul}}</h1>
  <p class="text-white">{{$film->ringkasan}}</p>
</div>

<div class="col-lg">
  <div class="login__form" style="width:995px;">
<h1 class="text-white" style="font-size:30px;">Komentar</h1>
@foreach ($film->komentar as $item)
    <div class="card mb-5">
        <div class="card-body">
          <small><b>{{$item->user->name}}</b></small>
          <p class="card-text">{{$item->content}}</p>
          <p class="card-text">{{$item->point}}</p>
        </div>
    </div>
@endforeach
<form action="/komentar" method="POST" enctype="multipart/form-data" style="margin-top:100px;">
    @csrf
    <div class="form-group">
      <div class="form-group">
        <label class="text-white">Content</label>
        <input type="text" name="film_id" value="{{$film->id}}" class="form-control mb-3" style="width:850px;">
        <label class="text-white">Tanggapan</label>
        <textarea name="content" class="form-control" cols="30" rows="10" style="width:850px;"></textarea>
      </div>
    </div>
    @error('content')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label class="text-white">Point</label>
        <input type="number" name='point' class="form-control" style="width:850px;">
      </div>
      @error('point')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="row login__register block">
        <button type="submit" class="primary-btn" style="width: 25%; height:50px; margin-left: 192px; margin-right:12px;">Submit</button>
        <a href="/film" class="btn btn-secondary" style="width: 25%; height:50px; margin-top:9px; line-height: 32px">Kembali</a>
      </div>
</form>
  </div>
</div>


@endsection