@extends('layout.master')
@section('judul')
Halaman Edit film 
@endsection    
@section('content')


@push('script')
<script src="https://cdn.tiny.cloud/1/yk0imf3zr5gkoy1klg43thkt9ooh0kfsw1w8nkvog26llb1z/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
  });
</script>
@endpush



<div class="col-lg">
  <div class="login__form">

    <form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="form-group">
        <label class="text-white">Judul film</label>
        <input type="text" name="judul" value="{{$film->judul}}" class="form-control" style="width:850px;">
      </div>
      @error('judul')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label class="text-white">Ringkasan film</label>
        <textarea name="ringkasan" cols="30" rows="10" class="form-control" style="width:850px;">{{$film->ringkasan}}</textarea>
      </div>
      @error('ringkasan')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label class="text-white">Tahun film</label>
        <input type="number" value="{{$film->tahun}}" name="tahun" class="form-control" style="width:850px;">
      </div>
      @error('tahun')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label class="text-white">Poster film</label>
        <input type="file" name="poster" class="form-control" style="width:850px;">
      </div>
      @error('poster')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <label class="text-white">Genre film</label> 
      <div class="form-group">
        <select name="genre_id" id="" >
          <option value="" class="form-control">---Pilih Genre---</option>
          @foreach ($genre as $item)
          @if ($item->id === $film->genre_id)
            <option value="{{$item->id}}" selected>{{$item->nama}}</option>
          @else
            <option value="{{$item->id}}">{{$item->nama}}</option>
          @endif
        @endforeach
  
          @error('genre_id')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          
        </select>
      </div>
      
      <div class="login__register">
        <button type="submit" class="primary-btn" style="margin-left: 220px;">Submit</button>
      </div>
    </form>
  </div>
</div>


@endsection
