<!-- Hero Section Begin -->
<section class="hero">
    <div class="container">
        <div class="hero__slider owl-carousel">
            <div class="hero__items set-bg" data-setbg="{{asset('admin/img/hero/hero-1.jpg')}}">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="hero__text">
                            <h2>Kelompok 1</h2>
                            <h2 style='font-size:28px;'>Website Anime</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Hero Section End -->

