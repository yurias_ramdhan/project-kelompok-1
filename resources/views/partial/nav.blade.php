<header class="header">
  <div class="container">
      <div class="row">
          <div class="col-lg-2">
              <div class="header__logo">
                  <a href="./index.html">
                      <img src="img/logo.png" alt="">
                  </a>
              </div>
          </div>
          <div class="col-lg-8">
              <div class="header__nav">
                <nav class="header__menu mobile-menu">
                    <ul>
                      <li><a href="/film">Film Anime</a></li>
                      <li><a href="/character">Character</a></li>
                      <li><a href="/genre">Genre</a></li>
                    
                    @auth
                    <li class="nav-item bg-danger text-white">
                      <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        <i style="height: 0;" aria-hidden="true"></i>
                        <p>
                          Logout
                        </p>
                      </a>
            
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                      </form>
                    </li>
                    @endauth
            
                    @guest
                <li class="nav-item bg-primary text-white">
                  <a href="/login">
                    <i style="height: 0px;"></i>
                    <p>
                      Login
                    </p>
                  </a>
                </li>
                    @endguest
                      </ul>
                  </nav>
              </div>
          </div>
          <div class="col-lg-2">
              <div class="header__right ">
                <a href="/" class="text-primary">Kelompok 1</a>
                  <img src="{{asset('admin/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2 rounded-full" alt="User Image" style="width:30px;">
              </div>
          </div>
      </div>
      <div id="mobile-menu-wrap"></div>
  </div>
</header>