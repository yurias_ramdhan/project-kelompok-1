<!-- Product Section Begin -->
<div class="col-3">
    <div class="product__item">
        <div class="product__item__pic set-bg" data-setbg="{{asset('admin/img/trending/trend-1.jpg')}}">
        </div>
        <div class="product__item__text">
            <ul>
                <li>Active</li>
                <li>Movie</li>
            </ul>
            <h5 class="mb-2"><a href="#">The Seven Deadly Sins: Wrath of the Gods</a></h5>
            <form action="#" method="POST">
                <a href="#" class="btn btn-primary btn-sm">Detail</a>
                <a href="#" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
        </div>
    </div>
</div>
<div class="col-3">
    <div class="product__item">
        <div class="product__item__pic set-bg" data-setbg="{{asset('admin/img/trending/trend-2.jpg')}}">
        </div>
        <div class="product__item__text">
            <ul>
                <li>Active</li>
                <li>Movie</li>
            </ul>
            <h5 class="mb-2"><a href="#">Gintama Movie 2: Kanketsu-hen - Yorozuya yo Eien</a></h5>
            <form action="#" method="POST">
                <a href="#" class="btn btn-primary btn-sm">Detail</a>
                <a href="#" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
        </div>
    </div>
</div>
<div class="col-3">
    <div class="product__item">
        <div class="product__item__pic set-bg" data-setbg="{{asset('admin/img/trending/trend-3.jpg')}}">
        </div>
        <div class="product__item__text">
            <ul>
                <li>Active</li>
                <li>Movie</li>
            </ul>
            <h5 class="mb-2"><a href="#">Shingeki no Kyojin Season 3 Part 2</a></h5>
            <form action="#" method="POST">
                <a href="#" class="btn btn-primary btn-sm">Detail</a>
                <a href="#" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
        </div>
    </div>
</div>
<div class="col-3">
    <div class="product__item">
        <div class="product__item__pic set-bg" data-setbg="{{asset('admin/img/trending/trend-4.jpg')}}">
        </div>
        <div class="product__item__text">
            <ul>
                <li>Active</li>
                <li>Movie</li>
            </ul>
            <h5 class="mb-2"><a href="#">Fullmetal Alchemist: Brotherhood</a></h5>
            <form action="#" method="POST">
                <a href="#" class="btn btn-primary btn-sm">Detail</a>
                <a href="#" class="btn btn-warning btn-sm">Edit</a>
                <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
        </div>
    </div>
</div>