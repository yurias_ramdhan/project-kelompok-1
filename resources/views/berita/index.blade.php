@extends('layout.master')
@section('judul')
Isi Berita
@endsection    
@section('content')

<a href="/berita/create" class="btn btn-secondary mb-3">Tambah Berita</a>

<table class="table" style="background-color: white">
    <thead class="thead-dark">
      <tr>
        <th scope="col">Id</th>
        <th scope="col">Judul</th>
        {{-- <th scope="col">Poster</th> --}}
        <th scope="col">Content</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($berita as $key => $item)
          <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->tulisan}}</td>
            <td>{{$item->rencana}}</td>
            <td>
              <form action="/berita/{{$item->id}}" method="POST">
                <a href="/berita/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/berita/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
              </form>
              @empty
                  <i><h1 style="color:white; font-size:50px; margin-left: 20px;">Data tidak ada</h1></i>
              @endforelse
            </td>
          </tr> 
            
    </tbody>
  </table>

@endsection