@extends('layout.master')
@section('judul')
Halaman Edit Berita
@endsection    
@section('content')

<div class="col-lg">
  <div class="login__form">

    <form action="/berita/{{$berita->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group mb-3">
          <label class="text-white">Nama Berita</label>
          <input type="text" name="tulisan" value="{{$berita->tulisan}}" class="form-control">
        </div>
        @error('tulisan')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
        <div class="form-group mb-3">
          <label class="text-white">Content Berita</label>
          <textarea name="rencana" class="form-control">{{$berita->rencana}}</textarea>
        </div>
        @error('rencana')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="row login__register">
          <button type="submit" class="primary-btn" style="width: 25%; height:50px; margin-left: 285px;">Submit</button>
        </div>
    </form>
  </div>
</div>
@endsection
