@extends('layout.master')
@section('judul')
Halaman Tambah Berita
@endsection 

@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>
@endpush

@section('content')

<div class="col-lg">
  <div class="login__form">

    <form action="/berita" method="POST">
        @csrf
        <div class="form-group mb-3">
          <label class="text-white">Nama Berita</label>
          <input type="text" name="tulisan" class="js-example-basic-single" style="width:850px;">
        </div>
        @error('tulisan')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group mb-3">
          <label class="text-white">Content</label>
          <textarea name="gambar" class="form-control" style="width:850px;"></textarea>
        </div>
        @error('gambar')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="row login__register">
          <button type="submit" class="primary-btn" style="width: 25%; height:50px; margin-left: 285px;">Submit</button>
        </div>
    </form>
  </div>
</div>
@endsection
