@extends('layout.master')
@section('judul')
Halaman Tambah Character
@endsection    
@section('content')


@push('script')
<script src="https://cdn.tiny.cloud/1/yk0imf3zr5gkoy1klg43thkt9ooh0kfsw1w8nkvog26llb1z/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
  });
</script>
@endpush


<div class="col-lg">
  <div class="login__form">

    <form action="/character" method="POST">
        @csrf
        <div class="form-group mb-3">
          <label class="text-white">Nama Character</label>
          <input type="text" name="nama" class="form-control" style="width:850px;">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group mb-3">
          <label class="text-white">Umur Character</label>
          <input type="number" name="umur" class="form-control" style="width:850px;">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group mb-3">
          <label class="text-white">Bio Character</label>
          <textarea name="bio" class="form-control" style="width:850px;"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="row login__register">
          <button type="submit" class="primary-btn" style="width: 25%; height:50px; margin-left: 285px;">Submit</button>
        </div>
    </form>
  </div>
</div>
@endsection
