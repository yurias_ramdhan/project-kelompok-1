@extends('layout.master')
@section('judul')
Detail Character
@endsection    
@section('content')

<div class="text-center" style="margin-left: 360px; margin-bottom: 100px;">
    <h4 class="text-white">Nama Character : {{$character->nama}}</h4>
    <h4 class="text-white">Umur Character : {{$character->umur}}</h4>
    <h4 class="text-white">Bio Character : {{$character->bio}}</h4>
    <a href="/character" class="btn btn-secondary" style="width: 25%; height:50px; margin-top:50px; line-height: 32px">Kembali</a>
</div>


@endsection