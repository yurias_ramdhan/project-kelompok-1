@extends('layout.master')
@section('judul')
Isi Character
@endsection    
@section('content')

<a href="/character/create" class="btn btn-secondary mb-3">Tambah Character</a>

<table class="table" style="background-color: white">
    <thead class="thead-dark">
      <tr>
        <th scope="col">Id</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($character as $key => $item)
          <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td>
            <td>
              <form action="/character/{{$item->id}}" method="POST">
                <a href="/character/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/character/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
              </form>
              @empty
                  <i><h1 style="color:white; font-size:50px; margin-left: 20px;">Data tidak ada</h1></i>
              @endforelse
            </td>
          </tr> 
            
    </tbody>
  </table>

@endsection