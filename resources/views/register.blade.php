@extends('layout.master')

@section('judul')
    Halaman Form
@endsection
    
@section('content')
    <h2>Buat Account Baru</h2>
        
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="post">
        @csrf
        <label>First name :</label> <br><br>
        <input type="text" name="first">
        <br><br>
        <label>Last name :</label> <br><br>
        <input type="text" name="last">
        <br><br>
        <label>Gender</label> <br><br>
        <input type="radio" name="gender"><label>Male</label> <br>
        <input type="radio" name="gender"><label>Female</label>
        <br><br>
        <label>Nationality</label> <br><br>
        <select name="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="amerika">Amerika</option>
            <option value="inggris">Inggris</option>
        </select>
        <br><br>
        <label>Language Spoken</label> <br><br>
        <input type="checkbox" name="language"><label>Bahasa Indonesia</label> <br>
        <input type="checkbox" name="language"><label>English</label> <br>
        <input type="checkbox" name="language"><label>Other</label>
        <br><br>
        <label>Bio</label> <br><br>
        <textarea name="bio" rows="10" cols="30"></textarea>
        <br><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection