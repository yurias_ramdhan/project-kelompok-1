<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Website Anime</title>
    

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('admin/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('admin/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('admin/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('admin/css/plyr.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('admin/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('admin/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('admin/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('admin/css/style.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('admin/plugins/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    
    @stack('style')

    @yield('style')
    <style>
        h4::after{
            display:none;
        }

        h4 {
            font-size: 35px;
        }

        div.login__form::after {
            display: none;
        }
    </style>

</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
    </div>
    <div class="loader"></div>
    
    @include('sweetalert::alert')

    <!-- Header Section Begin -->
    @include('partial.nav')
    <!-- Header End -->

    <!-- Hero Image Section Begin-->
    @include('partial.hero')
    <!-- Hero Image End-->

    
    <section class="product spad">
        <div class="container text-center">
            <div class="section-title">
                <h4 style="margin-bottom: 100px">@yield('judul')</h4>
            </div>
        </div>
        <div class="container">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </section>
    <!-- Product Section End -->

<!-- Footer Section Begin -->
<footer class="footer">
    <div class="page-up">
        <a href="#" id="scrollToTopButton"><span class="arrow_carrot-up"></span></a>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="footer__logo">
                    <a href="./index.html"><img src="{{asset('admin/img/logo.png')}}" alt=""></a>
                </div>
            </div>
            <div class="col-lg-7">
                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            </div>
          </div>
      </div>
  </footer>
  <!-- Footer Section End -->


<!-- Js Plugins -->
<script src="{{asset('admin/js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
<script src="{{asset('admin/js/player.js')}}"></script>
<script src="{{asset('admin/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('admin/js/mixitup.min.js')}}"></script>
<script src="{{asset('admin/js/jquery.slicknav.js')}}"></script>
<script src="{{asset('admin/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('admin/js/main.js')}}"></script>

@stack('script')
</body>

</html>