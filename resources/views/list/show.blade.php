@extends('layout.master')
@section('judul')
Halaman Detail List {{$list->judul}}
@endsection    
@section('content')

<img src="{{asset('gambar/'.$list->poster)}}" alt="">
<h1>{{$list->judul}}</h1>
<p>{{$list->ringkasan}}</p>

<h1>Komentar</h1>

@foreach ($list->komentar as $item)
    <div class="card">
        <div class="card-body">
          <small><b>{{$item->user->name}}</b></small>
          <p class="card-text">{{$item->content}}</p>
          <p class="card-text">{{$item->point}}</p>
        </div>
    </div>
@endforeach
<form action="/komentar" method="POST" enctype="multipart/form-data" class="my-3">
    @csrf
    <div class="form-group">
      <label>Content</label>
      <input type="hidden" name="list_id" value="{{$list->id}}">
      <textarea name="content" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('content')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Point</label>
        <input type="number" name='point' class="form-control">
      </div>
      @error('point')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<a href="/list" class="btn btn-secondary">Kembali</a>

@endsection