@extends('layout.master')
@section('judul')
Halaman List
@endsection    
@section('content')

@auth
  <a href="/list/create" class="btn btn-primary my-2">Tambah</a>
@endauth

<div class="row">
  @forelse ($list as $item)
    <div class="col-4">
      <div class="card bg-dark text-white">
        <img src="{{asset('gambar/'. $item->poster)}}" class="card-img-top" alt="...">
        <div class="card-body">
          <span class="badge badge-info">{{$item->genre->nama}}</span>
          <h3>{{$item->judul}}</h3>
          <p class="card-text"> {{Str::limit($item->ringkasan, 30)}} </p>
          @auth
            <form action="/list/{{$item->id}}" method="POST">
              @csrf
              @method('DELETE')
              <a href="/list/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
              <a href="/list/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
          @endauth

          @guest
            <a href="/list/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
          @endguest
        </div>
      </div>
    </div>
  @empty
      <h4>Data list Belum Ada</h4>
  @endforelse
</div>

@endsection

