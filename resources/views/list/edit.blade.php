@extends('layout.master')
@section('judul')
Halaman Edit List 
@endsection    
@section('content')

<form action="/list/{{$list->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Judul List</label>
      <input type="text" value="{{$list->judul}}" name="judul" class="form-control">
    </div>
    @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Ringkasan List</label>
      <textarea name="ringkasan" class="form-control" cols="30" rows="10">{{$list->ringkasan}}</textarea>
    </div>
    @error('ringkasan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Tahun List</label>
      <input type="number" value="{{$list->tahun}}" name="tahun" class="form-control">
    </div>
    @error('tahun')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Poster List</label>
      <input type="file" name="poster" class="form-control">
    </div>
    @error('poster')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Genre List</label> 
      <select name="genre_id" class="form-control">
        <option value="">---Pilih Genre---</option>
        @foreach ($genre as $item)
          @if ($item->id === $list->genre_id)
            <option value="{{$item->id}}" selected>{{$item->nama}}</option>
          @else
            <option value="{{$item->id}}">{{$item->nama}}</option>
          @endif
        @endforeach
      </select>
    </div>
    @error('genre_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
