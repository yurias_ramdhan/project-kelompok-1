@extends('layout.master')

@section('judul')
{{ __('Register') }}
@endsection
@section('content')

    <!-- Login Section Begin -->
    <div class="col-lg">
        <div class="login__form">
                <form action="{{ route('register') }}" method="post">
                    @csrf
                    <div class="input-group mb-3">
                        <input type="text" name="name" placeholder="Full Name" style="width: 40%; height:50px; margin-left: 215px;">
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-user"></span>
                          </div>
                        </div>
                      </div>
                      @error('name')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
              
                      <div class="input-group mb-3">
                        <input type="email" name='email' placeholder="Email" style="width: 40%; height:50px; margin-left: 215px;">
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                          </div>
                        </div>
                      </div>
                      @error('email')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
              
                      <div class="input-group mb-3">
                        <input type="password" name='password' placeholder="Password" style="width: 40%; height:50px; margin-left: 215px;">
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                          </div>
                        </div>
                      </div>
                      @error('password')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
              
                      <div class="input-group mb-3">
                        <input type="password" name='password_confirmation' placeholder="Retype password" style="width: 40%; height:50px; margin-left: 215px;">
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                          </div>
                        </div>
                      </div>
              
                      <div class="input-group mb-3">
                          <input type="number" name='umur' placeholder="Isi Umur Anda" style="width: 44%; height:50px; margin-left: 215px;">
                      </div>
                      @error('umur')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
              
                      <div class="input-group mb-3">
                          <textarea name="bio" placeholder="isi Biodata" style="width: 44%; height:100px; margin-left: 215px;"></textarea>
                      </div>
                      @error('bio')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
              
                      <div class="input-group mb-3">
                          <textarea name="alamat" placeholder="isi Alamat" style="width: 44%; height:100px; margin-left: 215px;"></textarea>
                      </div>
                      @error('alamat')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
              
              
                      <div class="row login__register">
                          <button type="submit" class="primary-btn" style="width: 25%; height:50px; margin-left: 285px;">Register</button>
                      </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Login Section End -->

@endsection

