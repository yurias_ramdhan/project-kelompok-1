@extends('layout.master')

@section('judul')
{{ __('Login') }}
@endsection
@section('content')

    <!-- Login Section Begin -->
    <div class="col-lg-6">
        <div class="login__form">
            <div class="card-body">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div>
                        <label for="email" style="margin-left: -10px; color: white; margin-right: 10px;">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email" class="@error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
            </div>
                    <div style="margin-left: 50px;">
                        <label style='margin-right:10px; color:white; margin-left:-40px;' for="password">{{ __('Password') }}</label>
                        <input id="password" type="password" style="margin-left:39px;" class="@error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group row mt-3" style="margin-left:140px;">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                    </div>

                    <div class="form-group row mb-0" style="margin-left: 140px;">
                        <button type="submit" class="btn btn-primary">
                                {{ __('Login') }}
                        </button>

                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        
                    </div>
                </form>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="login__register">
                <h3>Dont’t Have An Account?</h3>  
                <a href="{{ route('register') }}" class="primary-btn">Register Now</a>
            </div>
    </div>
    <!-- Login Section End -->

@endsection

