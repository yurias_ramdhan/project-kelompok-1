@extends('layout.master')
@section('judul')
Halaman Update Profile 
@endsection 
@push('script')
<script src="https://cdn.tiny.cloud/1/a2vko9p4067rrrvnygaufahn98faf4hlmjhmu69an6wnb3es/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush
@section('content')

<form action="/profile/{{$profile->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama User</label>
      <input type="text" value="{{$profile->user->name}}" class="form-control" disabled>
    </div>

    <div class="form-group">
        <label>Email User</label>
        <input type="text" value="{{$profile->user->email}}" class="form-control" disabled>
    </div>
      
      <div class="form-group">
        <label>Umur Profile</label>
        <input type="number" value="{{$profile->umur}}" name="umur" class="form-control">
      </div>
      @error('umur')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror

    <div class="form-group">
      <label>Biodata</label>
      <textarea name="bio" class="form-control" cols="30" rows="10">{{$profile->bio}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" class="form-control" cols="30" rows="10">{{$profile->alamat}}</textarea>
      </div>
    @error('alamat')
          <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection