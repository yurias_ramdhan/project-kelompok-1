@extends('layout.master')
@section('judul')
Isi Genre
@endsection    
@section('content')

<a href="/genre/create" class="btn btn-secondary mb-3">Tambah Genre</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">Id</th>
        <th scope="col">Nama</th>
        <th scope="col">Film</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody class="bg-white">
        @forelse ($genre as $key => $item)
          <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>
              <ul>
                @foreach ($item->film as $value)
                  <li>{{$value->judul}}</li>
                @endforeach
              </ul>
            </td>
            <td>
                <form action="/genre/{{$item->id}}" method="POST">
                    <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
          </tr> 
          @empty
          <i><h1 style="color:white; font-size:50px; margin-left: 20px;">Data tidak ada</h1></i>
      @endforelse
    </tbody>
  </table>

@endsection