@extends('layout.master')
@section('judul')
Halaman Tambah Genre
@endsection    
@section('content')

<div class="col-lg">
  <div class="login__form">
<form action="/genre" method="POST">
    @csrf
    <div class="form-group">
      <label class="text-white">Nama Genre</label>
      <input type="text" name="nama" class="form-control" style="width:850px;">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="row login__register">
      <button type="submit" class="primary-btn" style="width: 25%; height:50px; margin-left: 285px;">Submit</button>
    </div>
</form>
  </div>
</div>

@endsection
