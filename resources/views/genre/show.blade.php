@extends('layout.master')
@section('judul')
Detail Genre
@endsection    
@section('content')

<div class="text-center" style="margin-left: 350px; margin-bottom: 100px;">
  <h4 class="text-white">Nama Genre : {{$genre->nama}}</h4>
</div>



<div class="row">
@foreach ($genre->film as $item)
<div class="col-3">
  <div class="product__item" >
      <div class="product__item__pic" >
        <img src="{{asset('gambar/'. $item->poster)}}" width="1500px" style="border: 1px solid gray;">
      </div>
      <div class="product__item__text">
          <ul>
              <li></li>
          </ul>
          <h5 class="mb-2" style="color:white;">{{$item->judul}}</h5>
          <p class="card-text text-gray"> {{Str::limit($item->ringkasan, 30)}} </p>  
        </div>
      </div>
      <a href="/character" class="btn btn-secondary" style="width: 30%; height:50px; margin-top:50px; margin-left:540px; line-height: 32px">Kembali</a>
      
    </div>

@endforeach
</div>

@endsection